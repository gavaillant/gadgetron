/**
 * \file hoCuNDArray_operators.h
 * \brief Operators on the hoCuNDArray class. For now just delegates everything to hoNDArray operators.
 */

#pragma once

#include "hoNDArray_operators.h"

